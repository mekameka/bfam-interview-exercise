public class QuoteCalculationEngineImpl implements QuoteCalculationEngine {
	@Override
	public double calculateQuotePrice(int securityId, double referencePrice, boolean buy, int quantity) {
		// access to to libraries for pricing of quote requests.
		return someLibrary.calculateQuotePrice(securityId, referencePrice, buy, quantity);
	}
}

public class ReferencePriceSourceListener implements ReferencePriceSourceListener {
	@Override
	public void referencePriceChanged(int securityId, double price) {
		System.out.println("updating referencePrice for securityId" + securityId + ": " + price);
	}
}

public class ReferencePriceSourceImpl implements ReferencePriceSource {

	private ReferencePriceSourceListener listener = null;
	public Map<Integer, Double> referencePriceMap = new ConcurrentHashMap<>();

	@Override
	public void subscribe(ReferencePriceSourceListener listener) {
		this.listener = listener;
	}

	@Override
	public double get(int securityId) {
		// return -1 for security has no price yet
		referencePriceMap.getOrDefault(securityId, -1);
	}


	public void referencePriceChanged(int securityId, double price) {
		// check if listener is initialized
		if (listener != null) {
			listener.referencePriceChanged(securityId, price);
			referencePriceMap.put(securityId, price);
		} else {
			System.out.println("listener is not initialized!");
		}
	}
}


public class ProcessorFromClients implements MessageQueueProcessor {
	private ReferencePriceSourceImpl referencePriceSource = null;
	private QuoteCalculationEngine quoteCalculationEngine = null;

	// process request from clients
	private Processor (ReferencePriceSourceImpl referencePriceSource) {
		this.referencePriceSource = referencePriceSource;
		this.quoteCalculationEngine = new QuoteCalculationEngineImpl();
	}

	@Override 
	public void processor(DataRecord dataRecord) {
		// assume we have clientId in request from client
		String clientId = dataRecord.getAttribute("clientId");
		// assume we have msgFromClient in request like '123 BUY 100' from client
		String msgFromClient = dataRecord.getAttribute("msgFromClient");
		if (msgFromClient != null) {
			String[] msgArray = msgFromClient.split(' ');
			if (msgArray.length == 3) {
				int securityId = Integer.parse(msgArray[0]);
				boolean buy = "BUY".equals(msgArray[1]);
				int quantity = Integer.parse(msgArray[2]);
				double referencePrice = this.referencePriceSource.get(securityId);
				if (referencePrice != null) {
					double quotePrice = this.quoteCalculationEngine.calculateQuotePrice(securityId, referencePrice, buy, quantity);
					// Assume we have some kinda of publisher to publish to a topic that client is also listening to
					Publisher.publish(clientId, quotePrice);
				}
			}
		}
	}
}

public class ProcessorFromReferenceResource implements MessageQueueProcessor {
	private ReferencePriceSourceImpl referencePriceSource = null;
	// process price changes msgs from reference price source
	private Processor (ReferencePriceSourceImpl referencePriceSource) {
		this.referencePriceSource = referencePriceSource;
	}

	@Override 
	public void processor(DataRecord dataRecord) {
		// assume we have updated reference price for securityId in request from client
		int securityId = dataRecord.getAttribute("securityId");
		double referencePrice = dataRecord.getAttribute("referencePrice");
		this.referencePriceSource.referencePriceChanged(securityId, referencePrice);
	}
}


/**
 * The assumptions for this problem are listed as following:
 * 1. The communication between clients/referenceSource and servers is through third party message queue e.g. Solace
 * 2. There can be multiple clients and multiple servers
 * 3. Reference price resource keeps publishing reference price to a topic A that servers are listening to.
 * 4. Clients and servers are subscribing e.g. publishing/receving msgs to a message topic B and queue
 * 5. Processor implements some certain third-party specific message processor interface MessageQueueProcessor
 * 6. When server gets msgs from topic A (reference price source), it will use ProcessorFromReferenceResource to process the message
 * 7. When server gets msgs from topic B (clients), it will use ProcessorFromClients to process the message
 * 8. Multiple servers can be created based on group of security ids e.g. security ids of 1-1000 are assigned to server 1, 1001-2000 are assigned to server 2 etc
 * */